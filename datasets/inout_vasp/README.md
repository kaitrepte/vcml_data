### VASP inputs and outputs

Provided are:
- The geometries of all systems 
	- If optimizations were carried out, the final structure is provided 
	- For solids, the final five volumes around the minimum to obtain     
		      the EOS fit are given.     
		      volume_0 is the equilibrium structure
- The used input file (INCAR) containing the computational parameters 
- The used k-points (KPOINTS)
- The corresponding output files (OUTCAR)

Pseudopotentials are PAWs based on PBE, which are provided with any VASP contribution.    
We used VASP 5.4.4. 


Note that the functionals MCML and VCML will be available from VASP version 6 or later.
