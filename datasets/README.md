### Data sets

- values
	* Contains all values (all_values.dat) for all data sets and all functionals
	* Contains an evaluation script (eval.py) to extract all errors
- inout_vasp
	* Contains all VASP input and output files for all data sets and all functionals 

Contact:

+ Kai Trepte (kai.trepte1987@gmail.com)
