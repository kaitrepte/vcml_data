### Calculated values for all data sets. Reference values are provided as well.

all_values.dat

- Contains all values of all properties of all data sets
	+ DBH24
	+ RE42
	+ S66x8
	+ SOL62
	    * alat, lattice constants
	    * Ecoh, cohesive energies
	    * bulk, bulk moduli
	+ ADS41
	    * NOTE: Adsorption energies for ADS41 are normalized to the number of adsorbates
	+ W4-11
- Using a variety of functionals
  * PBE
  * PBE-D3
  * MS2
  * SCAN
  * r2SCAN
  * SCAN-rVV10
  * MCML
  * MCML-rVV10
  * VCML-rVV10

eval.py

- Evaluates the data in all_values.dat
- Generates tables and figures used in the corresponding publication

pics

- Stores generated figures

