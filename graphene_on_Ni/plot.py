# Original from Johannes Voss
# Adjusted by KT starting Oct. 26, 2021

import numpy as np
import glob
import pylab
import matplotlib
matplotlib.rc('font', size=30)
from VCML_ensemble import *


def readenergies(path):
    '''
        For all distances, read the energies of the combined
        systems as well as the isolated part. Then,
        compute the interaction energy per C atom.
    '''
    # Get all distances
    x = sorted(glob.glob(path+'/distance_*'))
    d = np.array([float(z.split('_')[-1]) for z in x])
    e = np.empty_like(d)
    for i,z in enumerate(x):
        f = open(z+'/energy.txt', 'r')
        e[i] = float(f.readline().strip())
        f.close()
    # get energies of isolated parts
    f = open(path+'/isolated_surface/energy.txt', 'r')
    e -= float(f.readline().strip())
    f.close()
    f = open(path+'/isolated_graphene/energy.txt', 'r')
    e -= float(f.readline().strip())
    f.close()
    # Compute final interaction energy
    e*=0.5
    return d,e


def main():
    '''
        Plot interaction energy of graphene on Ni.
        For VCML-rVV10, plot standard deviation based
        on Bayesian error analysis.
    '''
    #generate 5000 random functionals
    np.random.seed(123456)
    p = []
    for i in range(5000):
        perturb = np.dot(U_Lambda_minus_1_2, np.random.normal(size=61))
        p.append(theta_to_cij(VCML_theta+perturb))
    p = np.array(p)
    #read VCML exchange coefficients
    surfx   = np.loadtxt('VCML-rVV10/isolated_surface/xcoef.txt')
    graphx  = np.loadtxt('VCML-rVV10/isolated_graphene/xcoef.txt')
    both    = surfx+graphx
    xcoeffs = np.array([np.loadtxt(z+'/xcoef.txt')-both for z in sorted(glob.glob('VCML-rVV10/distance_*'))])
    
    ensemble = np.dot(xcoeffs,p.T)
    std      = np.std(ensemble, axis=1)
    # plot ensemble stuff
    dvcml,evcml = readenergies('VCML-rVV10')
    pylab.fill_between(dvcml, evcml-std, evcml+std, color='blue',alpha=0.05)
    
    # energies of all functionals
    for fun in (('PBE','green'),
                ('PBE-D3','darkgreen'),
                ('MS2','orange'),
                ('SCAN','red'),
                ('r2SCAN','darkred'),
                ('SCAN-rVV10','pink'),
                ('MCML','blue'),
                ('MCML-rVV10','purple'), 
                ('VCML-rVV10','lightblue')):
        d,e = readenergies(fun[0])
        if fun[0] == 'r2SCAN':
            label = r'r$^{2}$SCAN'
        else:
            label = fun[0]
        pylab.plot(d, e, 'o-', color=fun[1], label=label, markersize=4.9)
    
    # RPA data extracted from Kresse data
    rpa_distances   = np.array([1.93006,1.99855,2.08829,2.13490,2.15568,2.22507,2.29142,2.49402,2.58267,2.67183,2.73971,3.18707,3.30004,3.41250,3.75037,4.19714,5.32069,6.44276])
    rpa_interaction = np.array([6.2666,-35.3903,-60.4807,-65.1083,-66.1310,-64.3265,-59.4525,-44.9280,-42.7820,-42.8286,-44.4835,-59.4307,-60.8897,-59.9612,-51.9136,-37.9678,-17.2329,-10.4818])/1000. # in eV
    pylab.plot(rpa_distances,rpa_interaction,'--o',color='black',label='RPA')
    # black line, marking 0
    pylab.plot([1.5,6.0],[0.0,0.0],'-',color='black',linewidth=1.0)
    
    # Plot experimental values, ranging from 75 - 129 meV; at  2.1 +- 0.1 A
    pylab.plot([2.06,2.14],[-0.129,-0.129],'-',color='gray',linewidth=3.0)
    pylab.plot([2.10,2.10],[-0.129,-0.075],'-',color='gray',linewidth=3.0)
    pylab.plot([2.06,2.14],[-0.075,-0.075],'-',color='gray',linewidth=3.0,label='Experiment')
    pylab.plot([2.00,2.20],[-0.102,-0.102],'-',color='gray',linewidth=3.0)
    pylab.plot([2.00,2.00],[-0.097,-0.107],'-',color='gray',linewidth=3.0)
    pylab.plot([2.20,2.20],[-0.097,-0.107],'-',color='gray',linewidth=3.0)
    
    # Additional things for plot
    pylab.xlim((1.5,6))
    pylab.ylim((-0.205,0.045))
    pylab.legend(loc='lower right',frameon=False,ncol=2,columnspacing=1.0,borderpad=0.0,handletextpad=0.3)
    pylab.locator_params(axis='x', nbins=5)
    pylab.locator_params(axis='y', nbins=5)
    #pylab.xticks(fontsize=25)
    pylab.ylabel(r'$E_{\mathrm{interaction}}$/(C atom) (eV)')
    pylab.xlabel(r'Distance Gr$-$Ni(111) (${\rm \AA}$)')
    pylab.show()
    #pylab.savefig('GrNi.png', bbox_inches='tight')


if __name__ == '__main__':
    main()
