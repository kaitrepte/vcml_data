import os
from ase import Atom, Atoms
from ase.io import read
import numpy
import glob

def get_data_from_outcar():
    # Get total energy
    outcar_file = read('OUTCAR')
    Etot = outcar_file.get_total_energy()
    ffile = open('energy.txt','w')
    ffile.write(str(Etot))
    ffile.close()
    # Get X contributions
    ffile = open('OUTCAR','r')
    lines = ffile.readlines()
    ffile.close()
    Xcontributions = numpy.zeros(64)
    for l, line in enumerate(lines):
        if line == ' BEEF xc energy contributions 66\n':
           Xcontributions = numpy.zeros(64)
           for i in range(64):
               splitt = lines[l+i+1].split()
               Xcontributions[i] = float(splitt[2])
    # If no beef coeeficients -> all 0 (for all other functional)
    ffile = open('xcoef.txt','w')
    for xcontr in Xcontributions:
        ffile.write('{} \n'.format(xcontr))
    ffile.close()


all_distances = sorted(numpy.linspace(1.5,6.0,46).tolist()+numpy.linspace(2.01,2.31,50).tolist()) 
# Avoid double counting of number which are close, and the same when rounded.
# Here: 2.200
for dist in all_distances:
	if abs(dist-2.2) < 0.001:
		all_distances.remove(dist)

functional = ['PBE','PBE-D3','MS2','SCAN','r2SCAN','SCAN-rVV10','MCML','MCML-rVV10','VCML-rVV10']

for fun in functional:
    os.chdir(fun)
    # isolated systems
    for sys in ['isolated_surface','isolated_graphene']:
        os.chdir(sys)
        get_data_from_outcar()
        os.chdir('../')
    
    for dist in all_distances:
        try:
            os.chdir('distance_{:5.3f}'.format(dist))
            get_data_from_outcar()
            os.chdir('../')
        except: 
            continue
    os.chdir('../')
    
