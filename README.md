### VCML_data

van der Waals, constrained and machined learned (VCML) functional development    
+ Based on the work on a new vdW functional (VCML-rVV10)
+ Several data sets computed with a number of exchange-correlation functionals

- datasets
	* Contains all data of all data sets
	* Includes VASP inputs/outputs as well as all computed values 
- graphene_on_Ni
	* Contains all data points for the graphene@Ni111 study
	* Includes plot.py script for visualization

References:

+ Data-driven and constrained optimization of semi-local exchange and non-local correlation functionals for materials and surface chemistry    
+ [arXiv link](https://arxiv.org/abs/2201.11106)    

+ Available in libXC, see [GitHub](https://github.com/ElectronicStructureLibrary/libxc)

Contact:

+ Kai Trepte (kai.trepte1987@gmail.com)
